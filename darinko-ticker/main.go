package main // badc0de.net/pkg/darinko/contrib/darinko-ticker

import (
	"flag"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"strings"
	"time"

	pb "badc0de.net/pkg/darinko/contrib/proto"
	"badc0de.net/pkg/flagutil"

	"github.com/bdlm/log"
	"github.com/coreos/go-systemd/activation"
	"github.com/golang/glog"
	"github.com/leekchan/accounting"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"sourcegraph.com/sourcegraph/appdash"
	appdashot "sourcegraph.com/sourcegraph/appdash/opentracing"
)

var (
	systemdSocket           = flag.Bool("systemd_socket", false, "instead of --address, use systemd socket activation")
	address                 = flag.String("address", ":9078", "address to listen on")
	appdashCollectorAddress = flag.String("appdash_collector_address", "", "address of the appdash collector; if unspecified, tracing is not submitted to appdash")
	metricsAddress          = flag.String("metrics_address", "", "address to spin up the metrics / debug http server on; if empty then disabled")

	debugStockList = flag.String("debug_stocklist", "", "if nonempty, just fetch the comma separated list of stocks, print out and quit. will uppercase stock symbols.")
)

func init() {
	flagutil.Parse()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	if *debugStockList != "" {
		stocks := strings.Split(*debugStockList, ",")
		cli(stocks)
		return
	}

	serve()
}

func serve() {
	var s *grpc.Server
	unaryInterceptors := []grpc.UnaryServerInterceptor{}
	if *metricsAddress != "" {
		unaryInterceptors = append(unaryInterceptors, grpc_prometheus.UnaryServerInterceptor)
	}
	if *appdashCollectorAddress != "" {
		log.Infof("using appdash tracer %s", *appdashCollectorAddress)
		tracer := appdashot.NewTracer(appdash.NewRemoteCollector(*appdashCollectorAddress))
		opentracing.SetGlobalTracer(tracer)
		unaryInterceptors = append(unaryInterceptors, otgrpc.OpenTracingServerInterceptor(tracer))
	}
	if len(unaryInterceptors) == 0 {
		s = grpc.NewServer()
	} else {
		s = grpc.NewServer(grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(unaryInterceptors...)))
	}
	reflection.Register(s)

	srv := &server{}
	pb.RegisterChatBotServer(s, srv)

	if *systemdSocket {
		glog.Infof("using systemd socket activation")
		listeners, err := activation.ListenersWithNames()
		if err != nil {
			glog.Fatalf("cannot retrieve listeners: %s", err)
		}

		// TODO: add graceful server shutdown.
		// See http://web.archive.org/web/20201201180140/https://vincent.bernat.ch/en/blog/2018-systemd-golang-socket-activation
		stopper := make(chan interface{})
		for name, lis := range listeners {
			switch name {
			case "darinko-be-ticker-metrics.socket":
				grpc_prometheus.Register(s)
				http.Handle("/metrics", promhttp.Handler())
				go func() {
					glog.Fatalf("failed to serve metrics: %v", http.Serve(lis[0], nil))
					stopper <- name
				}()
			case "darinko-be-ticker-grpc.socket":
				glog.Infof("beginning systemd activated serving")
				go func() {
					glog.Fatalf("failed to serve grpc: %v", s.Serve(lis[0]))
					stopper <- name
				}()
			}
		}

		// TODO: use wg.Wait() instead.
		for range listeners {
			<-stopper
		}
		glog.Infof("all listeners stopped")
	} else {

		if *metricsAddress != "" {
			grpc_prometheus.Register(s)
			http.Handle("/metrics", promhttp.Handler())
			go http.ListenAndServe(*metricsAddress, nil)
		}

		lis, err := net.Listen("tcp", *address)
		if err != nil {
			glog.Fatalf("failed to listen: %v", err)
		}

		glog.Infoln("starting to serve darinko-ticker")
		if err := s.Serve(lis); err != nil {
			glog.Fatalf("failed to serve: %v", err)
		}
	}
}

func withPlus(ac *accounting.Accounting, num float64) string {
	sign := func(x float64) string {
		switch {
		case x > 0:
			return "+"
		case x == 0:
			return " "
		default:
			return "" // already in %f
		}
	}
	if ac != nil {
		return fmt.Sprintf("%s%s", sign(num), ac.FormatMoneyFloat64(num))
	} else {
		return fmt.Sprintf("%s%.02g", sign(num), num)
	}
}

func cli(s []string) {
	stocks, err := GetStocks(s)
	if err != nil {
		panic(err)
	}
	ac := accounting.DefaultAccounting("$", 2)
	for _, s := range stocks {
		fmt.Printf(" * %s\t$%s\t%s\t%s%%\n", s.Symbol, s.RegularMarketPrice, withPlus(ac, s.RegularMarketChange), withPlus(nil, s.RegularMarketChangePercent))
	}
}
