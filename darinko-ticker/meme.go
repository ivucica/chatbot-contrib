package main

import (
	"fmt"
	"math/rand"
	"strings"
)

var (
	memeStocks = map[string]bool{
		"GME":  true,
		"AMC":  true,
		"BB":   true,
		"BBBY": true,
		"NOK":  true,
	}
)

func generateEmoji() string {
	monkees := []string{"🐒", "🦧", "🦍"}
	picks := []string{
		"#HODL",
		"#APESTRONG",
		"💎🙌",
		"🚀🚀🚀🚀",
		fmt.Sprintf("%s💪💪💪", monkees[rand.Intn(len(monkees))]),
	}
	var out []string
	for i := 0; i < 1+rand.Intn(3); i++ {
		out = append(out, picks[rand.Intn(len(picks))])
	}

	return strings.Join(out, " ")
}

func isMeme(symbol string) bool {
	_, isMeme := memeStocks[strings.ToUpper(symbol)]
	return isMeme
}
