package main // badc0de.net/pkg/darinko/contrib/darinko-ticker

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/mitchellh/mapstructure"
)

const (
	// https://github.com/pstadler/ticker.sh/blob/master/ticker.sh
	apiEndpoint = "https://query1.finance.yahoo.com/v7/finance/quote?lang=en-US&region=US&corsDomain=finance.yahoo.com"
)

var (
	fields = []string{
		"symbol",
		"marketState",
		"regularMarketPrice",
		"regularMarketChange",
		"regularMarketChangePercent",
		"preMarketPrice",
		"preMarketChange",
		"preMarketChangePercent",
		"postMarketPrice",
		"postMarketChange",
		"postMarketChangePercent",
	}
)

type Stock struct {
	EsgPopulated               bool
	Exchange                   string
	ExchangeDataDelayedBy      float64
	ExchangeTimezoneName       string
	ExchangeTimezoneShortName  string
	FirstTradeDateMilliseconds float64
	FullExchangeName           string
	GmtOffSetMilliseconds      float64
	Language                   string
	Market                     string
	MarketState                string

	PreMarketChange         float64
	PreMarketChangePercent  float64
	PreMarketPrice          float64
	PreMarketTime           float64
	PostMarketChange        float64
	PostMarketChangePercent float64
	PostMarketPrice         float64
	PostMarketTime          float64

	PriceHint                  float64
	QuoteSourceName            string
	QuoteType                  string
	Region                     string
	RegularMarketChange        float64
	RegularMarketChangePercent float64
	RegularMarketPreviousClose float64
	RegularMarketPrice         float64
	RegularMarketTime          float64
	SourceInterval             float64
	Symbol                     string
	Tradeable                  bool
	Triggerable                bool

	Currency string
}

type UA struct {
	UA   string
	Next http.RoundTripper
}

func (ua *UA) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Header.Set("User-Agent", ua.UA)
	return ua.Next.RoundTrip(r)
}

type Accept struct {
	Accept string
	Next   http.RoundTripper
}

func (a *Accept) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Header.Set("Accept", a.Accept)
	return a.Next.RoundTrip(r)
}

type BadJSON struct {
	Err     error
	Comment string
}

func (e *BadJSON) Error() string {
	if e.Err != nil {
		return fmt.Sprintf("bad json: %s: %v", e.Comment, e.Err)
	} else {
		return fmt.Sprintf("bad json: %s", e.Comment)
	}
}

type HTTPError struct {
	Err error
}

func (e *HTTPError) Error() string {
	return fmt.Sprintf("http error: %s: %v", e.Err)
}

func GetStocks(symbols []string) ([]Stock, error) {
	fields := strings.Join(fields, ",")

	client := &http.Client{
		Transport: &UA{
			// see also: http://www.bizcoder.com/the-much-maligned-user-agent-header
			UA: "DarinkoTicker/1.0 (contrib; Darinko/1.0; a toy chatbot for a small community; +https://badc0de.net/darinko)",
			Next: &Accept{
				Accept: "application/json",
				Next:   &http.Transport{},
			},
		},
	}

	resp, err := client.Get(apiEndpoint + "&fields=" + fields + "&symbols=" + strings.Join(symbols, ","))
	if err != nil {
		return nil, &HTTPError{Err: err}
	}
	if resp.StatusCode != http.StatusOK {
		return nil, &HTTPError{Err: fmt.Errorf("status not ok")}
	}
	if ct := resp.Header.Get("Content-Type"); ct != "application/json" {
		fmt.Fprintf(os.Stderr, "warning: content type: got %s, want appplication/json\n", ct)
	}
	defer resp.Body.Close()

	dec := json.NewDecoder(resp.Body)
	ret := make(map[string]interface{})
	if err := dec.Decode(&ret); err != nil {
		return nil, &BadJSON{Comment: "err decoding json", Err: err}
	}

	qrI, ok := ret["quoteResponse"]
	if !ok {
		return nil, &BadJSON{Comment: "no quoteResponse in json"}
	}
	qr, ok := qrI.(map[string]interface{})
	if !ok {
		return nil, &BadJSON{Comment: "quoteResponse is not a dict"}
	}
	if e, ok := qr["error"]; ok && e != nil && e != "" {
		return nil, &BadJSON{Comment: fmt.Sprintf("error returned service: %v", e)}
	}

	resI, ok := qr["result"]
	if !ok {
		return nil, &BadJSON{Comment: "no result not quoteResponse"}
	}
	res, ok := resI.([]interface{})
	if !ok {
		return nil, &BadJSON{Comment: "result is not an array"}
	}

	var stocks []Stock
	for _, stockI := range res {
		var stock Stock
		if err := mapstructure.Decode(stockI, &stock); err != nil {
			return nil, &BadJSON{Comment: "failed to put a stock into the structure", Err: err}
		}

		stocks = append(stocks, stock)
	}

	return stocks, nil

}
