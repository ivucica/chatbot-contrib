package main // badc0de.net/pkg/darinko/contrib/darinko-ticker

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"github.com/leekchan/accounting"
	"golang.org/x/text/currency"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "badc0de.net/pkg/darinko/contrib/proto"
)

type server struct {
	pb.UnimplementedChatBotServer
}

func (s *server) Respond(req *pb.RespondRequest, stream pb.ChatBot_RespondServer) error {
	if req.GetChatMessage() == nil {
		return status.Errorf(codes.InvalidArgument, "no chat message")
	}
	if req.GetChatMessage().GetBody() == "" {
		return status.Errorf(codes.InvalidArgument, "empty chat message")
	}
	glog.Infof("message from %q (using %q): %q", req.ChatMessage.Source.Address, req.ChatMessage.Source.ChatProtocol, req.ChatMessage.Body)
	var stocks []string
	if body := req.GetChatMessage().GetBody(); body[0] == '$' {
		stock := body[1:]
		if len(stock) == 0 {
			return status.Errorf(codes.InvalidArgument, "stock length is zero")
		}
		if occurrence := strings.Index(stock, " "); occurrence >= 0 {
			stock = strings.ToUpper(stock[:occurrence])
		}
		stocks = []string{stock}
	}
	if body := req.GetChatMessage().GetBody(); body[0] == '.' {
		cmdLine := body[1:]
		argv := strings.Split(cmdLine, " ")
		switch argv[0] {
		case "stock":
			if len(argv) == 1 {
				return s.respond(req, stream, "no stocks specified")
			}
			for _, stock := range argv[1:] {
				stocks = append(stocks, strings.ToUpper(stock))
			}
		default:
			if !isMeme(argv[0]) {
				return status.Errorf(codes.InvalidArgument, "unknown command")
			}
			if len(argv) > 1 {
				return status.Errorf(codes.InvalidArgument, "too many arguments")
			}
			stocks = append(stocks, strings.ToUpper(argv[0]))
		}
	}

	if len(stocks) == 0 {
		return status.Errorf(codes.InvalidArgument, "no stocks")

	}

	var body string
	stockData, err := GetStocks(stocks)
	if err != nil {
		glog.Errorf("could not fetch stocks: %v", err)
		return s.respond(req, stream, "error fetching data, more info in logs")
	}

	for _, stock := range stockData {
		var ac *accounting.Accounting
		if stock.Currency == "" {
			ac = accounting.DefaultAccounting("$", 2)
		} else {
			if curr, err := currency.ParseISO(stock.Currency); err != nil {
				ac = accounting.DefaultAccounting(stock.Currency, 2)
			} else {
				// This SHOULD convert to a single character of currency symbol, where available.
				sym := curr.String()
				// But it doesn't. TODO: figure out why.
				switch sym {
				case "USD":
					sym = "$"
				case "EUR":
					sym = "€"
				case "GBP":
					sym = "£"
				}

				// Add space so value is not crammed.
				if utf8.RuneCountInString(sym) > 1 {
					sym += " "
				}
				ac = accounting.DefaultAccounting(sym, 2)
			}
		}

		suffix := ""
		if isMeme(stock.Symbol) {
			suffix = " " + generateEmoji()
		}

		switch stock.MarketState {
		case "REGULAR":
			// no op
		case "CLOSED":
			fallthrough
		case "POSTPOST":
			fallthrough
		case "POST":
			suffix = fmt.Sprintf(" -- postmarket: %s (%s %s%%) %s", ac.FormatMoneyFloat64(stock.PostMarketPrice), withPlus(ac, stock.PostMarketChange), withPlus(nil, stock.PostMarketChangePercent), suffix)
		case "PRE":
			suffix = fmt.Sprintf(" -- premarket: %s (%s %s%%) %s", ac.FormatMoneyFloat64(stock.PreMarketPrice), withPlus(ac, stock.PreMarketChange), withPlus(nil, stock.PreMarketChangePercent), suffix)
		}

		body = fmt.Sprintf("%s%s: %s (%s %s%%)%s\n", body, stock.Symbol, ac.FormatMoneyFloat64(stock.RegularMarketPrice), withPlus(ac, stock.RegularMarketChange), withPlus(nil, stock.RegularMarketChangePercent), suffix)
	}
	body = strings.TrimSpace(body)

	return s.respond(req, stream, body)
}

func (s *server) respond(req *pb.RespondRequest, stream pb.ChatBot_RespondServer, body string) error {
	var newDest *pb.ChatAddress
	if !req.GetChatMessage().GetIsGroupChat() {
		newDest = &pb.ChatAddress{
			ChatProtocol: req.GetChatMessage().GetSource().GetChatProtocol(),
			Address:      req.GetChatMessage().GetSource().GetAddress(),
		}
	} else {
		switch req.GetChatMessage().GetSource().GetChatProtocol() {
		case pb.ChatProtocol_XMPP:
			// Assume XMPP and remove the nickname.
			components := strings.Split(req.GetChatMessage().GetSource().GetAddress(), "/")
			muc := strings.Join(components[:len(components)-1], "/")
			newDest = &pb.ChatAddress{
				ChatProtocol: req.GetChatMessage().GetSource().GetChatProtocol(),
				Address:      muc,
			}
		case pb.ChatProtocol_TELEGRAM_BOT:
			newDest = &pb.ChatAddress{
				ChatProtocol: req.GetChatMessage().GetSource().GetChatProtocol(),
				Address:      req.GetChatMessage().GetSource().GetAddress(),
			}
		case pb.ChatProtocol_TELEGRAM_INLINE_QUERY:
			newDest = &pb.ChatAddress{
				ChatProtocol: req.GetChatMessage().GetSource().GetChatProtocol(),
				Address:      req.GetChatMessage().GetSource().GetAddress(),
			}
		default:
			return status.Errorf(codes.InvalidArgument, "unable to handle groupchat messages from source protocols other than XMPP or Telegram bot")
		}

	}

	return stream.Send(&pb.RespondResponse{
		ChatMessage: &pb.ChatMessage{
			InReplyTo:       req.GetChatMessage().GetMessageId(),
			CreateTime:      ptypes.TimestampNow(),
			OriginInReplyTo: req.GetChatMessage().GetOriginMessageId(),
			Destination:     newDest,
			Source:          req.GetChatMessage().GetDestination(),
			IsGroupChat:     req.GetChatMessage().GetIsGroupChat(),
			MessageId:       uuid.New().String(),
			ThreadId:        req.GetChatMessage().GetThreadId(),
			Body:            body,
		},
	})
}
