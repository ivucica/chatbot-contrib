# baila

Use python 2.x for now.

## Install individual packages

```
# optional: use virtual environment
# (alternatively, replace pip install below with either sudo pip install, or pip install --user)
virtualenv env  # skip if env already exists
. env/bin/activate

pip install protobuf grpc grpcio-reflection futures
```

## Install using requirements.txt

Requirements file was produced using `pip freeze > requirements.txt`.

Assuming you are in the `chatbot-contrib` or `contrib` root (level above `baila`):

```
virtualenv env  # skip if env already exists
. env/bin/activate

pip install -r baila/requirements.txt
```

## Run

Assuming you are in the `chatbot-contrib` or `contrib` root (level above `baila`):

```
. env/bin/activate

LISTEN_ADDRESS=:1234
# or LISTEN_ADDRESS=191.191.191.191:1234

python -m baila -g ${LISTEN_ADDRESS}
```
