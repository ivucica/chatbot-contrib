#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import random


#returns a dictionary with a  message to be sent if body matches a regex , or 0 if it doesnt
#
#current triggers are .baila and .nick, .mamic, .vojislav
#


def parseMsg(mfrom = "", body = "", trigExp = ""):
		
	if (re.search('^\.baila$', body)):
		random.seed()

		links = [
		        "https://youtu.be/RFaatpzqll0",
		        "https://youtu.be/bThSI666jJY",
		        "https://youtu.be/2uRLJZxINAQ",
		        "https://youtu.be/ZHVdScgUm7U",
		        "https://youtu.be/DlOO-EkdTCM",
		        "https://youtu.be/F9E8nIKjkYc",
		        "https://youtu.be/be2ZHZuiC18",
		        "https://youtu.be/YTbLPvYBK7E",
		        "https://youtu.be/f-LdY8rxckg",
		        "https://youtu.be/maL3fkT7KDo",
		        "https://youtu.be/Ps36cpj4YLI",
		        "https://youtu.be/edQ7tBzOR7Y",
		        "https://youtu.be/4N_oB92H9MA",
		        "https://youtu.be/s-psx_lEq10"
		        ]   
		outputDict = {
			"dest":mfrom,
			"body":random.choice(links)
		}

		return outputDict

		
	if (re.search('^\.nick$', body)):

		body = [
		        "bembara je stroj, kume",
		        "nema kmice bez M-trice",
		        "pumpa saund sistem, benbara se vozi",
				"nema do svabe",
				"ajkula asfalt razdire",
		        "Sjedala su vlazna, ali razlog mi je jasan",
				"odi u svoj 1.6tdi kmetino",
				"davim se u ameli kurcu",
				"najvise volim sobov kobas"
		        ]   
		outputDict = {
			"dest":mfrom,
			"body":random.choice(body)
		}

		return outputDict


		
	if (re.search('^\.mamic$', body)):

		body = [
		        "Ja tebe pamtim cijelo vrijeme kao lazova i kao jednog od vodecih falsifikatora. ",
		        "Tvoj je cilj napraviti predstavu, ti si mucki provokator.",
				"I zapamti, koga sam ja prokleo, a tebe proklinjem, taj nije dobro zavrsio. ",
				"Djubre! Mucko djubre!",
		        "Prestani vise lagati ovaj napaceni narod.",
				"Ne znas se ni oprati, zmazan si, prljav, smrdis, neskolovan i lazes!",
		        "Prestani vise lagati ovaj napaceni narod.",
		        " Proklet bio na Bozic 2010.!",
		        " zatvorite kafic, razbijte hotel... Dinamo i ja sve cemo platiti.",
		        "I ovaj YouTube nabijem isto na kurac.",
		        "Cuo sam da vi jedan drugom u redakciji ispod stola pusite svaki dan.",
				"Vucibatino jedna, vucibatino jedna, klosaru!",
				"DJUBRE!"
		        ]   
		outputDict = {
			"dest":mfrom,
			"body":random.choice(body)
		}

		return outputDict
	if (re.search('^\.vojislav$', body)):

		body = [
		        "Sto niste sve procitali gospodine Nice, vi lepse citate",
		        "Vi svi pripadnici sekretarijata haskog tribunala mozete samo da prihvatite da mi popusite kurac",
		        "Vi samo dalje ometajte pripremu moje odbrane, pa ce te na kraju morati da pojedete sva govna koja ste izasrali",
				"Jebem vam majku svima, pocevsi od Hansa Holcijusa nadalje",
				"Ne, ne mogu samo poslednji, moram i pred poslednji i poslednji zajedno",
		        "Ja sam srpskom narodu obecao da cu realizovati neki projekat sa holandskom kraljicom"
		        ]   
		outputDict = {
			"dest":mfrom,
			"body":random.choice(body)
		}

		return outputDict

	if (re.search('^\.3dpd$', body)):
		body = [
			"https://i.kym-cdn.com/photos/images/newsfeed/001/098/556/852.jpg",
			]
		outputDict = {
			"dest": mfrom,
			"body": random.choice(body),
		}
		return outputDict
	if (re.search('^\.2dpd$', body)):
		body = [
			'https://www.rcsb.org/structure/2dpd',
		]
		outputDict = {
			'dest': mfrom,
			'body': random.choice(body),
		}
		return outputDict
		
	if (re.search('^\.kosovo$', body)):
		body = "је срце Србије!"
		outputDict = {
			'dest': mfrom,
			'body': body,
		}
		return outputDict
	return 0



if __name__ == "__main__":
	out = parseMsg("",".nick", "^\.baila")
	if(out):
		print(out)
