#!/bin/bash
if [[ ! -e env ]] ; then
	python2.7 -m virtualenv env
fi
. env/bin/activate
pip install -r baila/requirements.txt
