# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: munger.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2
import chatbot_pb2 as chatbot__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='munger.proto',
  package='net.badc0de.darinko.proto',
  syntax='proto3',
  serialized_options=_b('Z\035badc0de.net/pkg/darinko/proto'),
  serialized_pb=_b('\n\x0cmunger.proto\x12\x19net.badc0de.darinko.proto\x1a\x1cgoogle/api/annotations.proto\x1a\rchatbot.proto\"{\n\x0cMungeRequest\x12<\n\x0c\x63hat_message\x18\x01 \x03(\x0b\x32&.net.badc0de.darinko.proto.ChatMessage\x12\x0c\n\x04name\x18\x02 \x01(\t\x12\x1f\n\x17swap_source_destination\x18\x03 \x01(\x08\"q\n\rMungeResponse\x12<\n\x0c\x63hat_message\x18\x01 \x03(\x0b\x32&.net.badc0de.darinko.proto.ChatMessage\x12\"\n\x1aswapped_source_destination\x18\x02 \x01(\x08\"\x14\n\x12ListMungersRequest\"H\n\x13ListMungersResponse\x12\x31\n\x06munger\x18\x01 \x03(\x0b\x32!.net.badc0de.darinko.proto.Munger\"\x16\n\x06Munger\x12\x0c\n\x04name\x18\x01 \x01(\t2\xae\x02\n\x0cMungerServer\x12\x99\x01\n\x05Munge\x12\'.net.badc0de.darinko.proto.MungeRequest\x1a(.net.badc0de.darinko.proto.MungeResponse\"=\x82\xd3\xe4\x93\x02\x37\"\x11/v1/mungers:munge:\x01*Z\x1f\"\x1a/v1/{name=mungers/*}:munge:\x01*\x12\x81\x01\n\x0bListMungers\x12-.net.badc0de.darinko.proto.ListMungersRequest\x1a..net.badc0de.darinko.proto.ListMungersResponse\"\x13\x82\xd3\xe4\x93\x02\r\x12\x0b/v1/mungersB\x1fZ\x1d\x62\x61\x64\x63\x30\x64\x65.net/pkg/darinko/protob\x06proto3')
  ,
  dependencies=[google_dot_api_dot_annotations__pb2.DESCRIPTOR,chatbot__pb2.DESCRIPTOR,])




_MUNGEREQUEST = _descriptor.Descriptor(
  name='MungeRequest',
  full_name='net.badc0de.darinko.proto.MungeRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='chat_message', full_name='net.badc0de.darinko.proto.MungeRequest.chat_message', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='net.badc0de.darinko.proto.MungeRequest.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='swap_source_destination', full_name='net.badc0de.darinko.proto.MungeRequest.swap_source_destination', index=2,
      number=3, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=88,
  serialized_end=211,
)


_MUNGERESPONSE = _descriptor.Descriptor(
  name='MungeResponse',
  full_name='net.badc0de.darinko.proto.MungeResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='chat_message', full_name='net.badc0de.darinko.proto.MungeResponse.chat_message', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='swapped_source_destination', full_name='net.badc0de.darinko.proto.MungeResponse.swapped_source_destination', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=213,
  serialized_end=326,
)


_LISTMUNGERSREQUEST = _descriptor.Descriptor(
  name='ListMungersRequest',
  full_name='net.badc0de.darinko.proto.ListMungersRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=328,
  serialized_end=348,
)


_LISTMUNGERSRESPONSE = _descriptor.Descriptor(
  name='ListMungersResponse',
  full_name='net.badc0de.darinko.proto.ListMungersResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='munger', full_name='net.badc0de.darinko.proto.ListMungersResponse.munger', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=350,
  serialized_end=422,
)


_MUNGER = _descriptor.Descriptor(
  name='Munger',
  full_name='net.badc0de.darinko.proto.Munger',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='net.badc0de.darinko.proto.Munger.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=424,
  serialized_end=446,
)

_MUNGEREQUEST.fields_by_name['chat_message'].message_type = chatbot__pb2._CHATMESSAGE
_MUNGERESPONSE.fields_by_name['chat_message'].message_type = chatbot__pb2._CHATMESSAGE
_LISTMUNGERSRESPONSE.fields_by_name['munger'].message_type = _MUNGER
DESCRIPTOR.message_types_by_name['MungeRequest'] = _MUNGEREQUEST
DESCRIPTOR.message_types_by_name['MungeResponse'] = _MUNGERESPONSE
DESCRIPTOR.message_types_by_name['ListMungersRequest'] = _LISTMUNGERSREQUEST
DESCRIPTOR.message_types_by_name['ListMungersResponse'] = _LISTMUNGERSRESPONSE
DESCRIPTOR.message_types_by_name['Munger'] = _MUNGER
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

MungeRequest = _reflection.GeneratedProtocolMessageType('MungeRequest', (_message.Message,), dict(
  DESCRIPTOR = _MUNGEREQUEST,
  __module__ = 'munger_pb2'
  # @@protoc_insertion_point(class_scope:net.badc0de.darinko.proto.MungeRequest)
  ))
_sym_db.RegisterMessage(MungeRequest)

MungeResponse = _reflection.GeneratedProtocolMessageType('MungeResponse', (_message.Message,), dict(
  DESCRIPTOR = _MUNGERESPONSE,
  __module__ = 'munger_pb2'
  # @@protoc_insertion_point(class_scope:net.badc0de.darinko.proto.MungeResponse)
  ))
_sym_db.RegisterMessage(MungeResponse)

ListMungersRequest = _reflection.GeneratedProtocolMessageType('ListMungersRequest', (_message.Message,), dict(
  DESCRIPTOR = _LISTMUNGERSREQUEST,
  __module__ = 'munger_pb2'
  # @@protoc_insertion_point(class_scope:net.badc0de.darinko.proto.ListMungersRequest)
  ))
_sym_db.RegisterMessage(ListMungersRequest)

ListMungersResponse = _reflection.GeneratedProtocolMessageType('ListMungersResponse', (_message.Message,), dict(
  DESCRIPTOR = _LISTMUNGERSRESPONSE,
  __module__ = 'munger_pb2'
  # @@protoc_insertion_point(class_scope:net.badc0de.darinko.proto.ListMungersResponse)
  ))
_sym_db.RegisterMessage(ListMungersResponse)

Munger = _reflection.GeneratedProtocolMessageType('Munger', (_message.Message,), dict(
  DESCRIPTOR = _MUNGER,
  __module__ = 'munger_pb2'
  # @@protoc_insertion_point(class_scope:net.badc0de.darinko.proto.Munger)
  ))
_sym_db.RegisterMessage(Munger)


DESCRIPTOR._options = None

_MUNGERSERVER = _descriptor.ServiceDescriptor(
  name='MungerServer',
  full_name='net.badc0de.darinko.proto.MungerServer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=449,
  serialized_end=751,
  methods=[
  _descriptor.MethodDescriptor(
    name='Munge',
    full_name='net.badc0de.darinko.proto.MungerServer.Munge',
    index=0,
    containing_service=None,
    input_type=_MUNGEREQUEST,
    output_type=_MUNGERESPONSE,
    serialized_options=_b('\202\323\344\223\0027\"\021/v1/mungers:munge:\001*Z\037\"\032/v1/{name=mungers/*}:munge:\001*'),
  ),
  _descriptor.MethodDescriptor(
    name='ListMungers',
    full_name='net.badc0de.darinko.proto.MungerServer.ListMungers',
    index=1,
    containing_service=None,
    input_type=_LISTMUNGERSREQUEST,
    output_type=_LISTMUNGERSRESPONSE,
    serialized_options=_b('\202\323\344\223\002\r\022\013/v1/mungers'),
  ),
])
_sym_db.RegisterServiceDescriptor(_MUNGERSERVER)

DESCRIPTOR.services_by_name['MungerServer'] = _MUNGERSERVER

# @@protoc_insertion_point(module_scope)
